import { Config, IConfigProperty } from "@radic/util";
import { cli, CliConfig, command, CommandArguments, inject, container, lazyInject, Log, OutputHelper, Cli } from 'radical-console'
import { DatabasePlugin } from 'radical-console-database'
import { existsSync, mkdirSync, readFileSync } from "fs";
import * as _ from "lodash";
import { paths } from "./paths";
import { DiscbotConfiguration, IConfig } from "../interfaces";
import { Database } from "radical-console-database";
import { writeFileSync } from 'fs'
import { resolve } from "path";
import { CommandGroup, CommandoClient } from "discord.js-commando";
import { Sqlite3SettingProvider } from "../database/providers/sqlite3";


export class Bootstrapper {
    protected config: IConfig;
    protected database: Database;
    protected client: CommandoClient;

    @lazyInject('cli.log') protected log: Log;

    protected constructor() {
        this.initHomeDir();
        this.config   = this.initConfig();
        this.database = this.initDatabase();
    }

    public static boot(): Bootstrapper {
        return new Bootstrapper();
    }

    protected initHomeDir() {
        if ( ! existsSync(paths.homeDir) ) {
            mkdirSync(paths.homeDir);
        }
    }

    protected initConfig(): IConfig {
        let baseConfig: DiscbotConfiguration = {}
        paths.rcFiles.forEach(filePath => {
            if ( existsSync(filePath) ) {
                _.merge(baseConfig, JSON.parse(readFileSync(filePath, { encoding: 'utf-8' })))
            }
        })
        const _config         = new Config(<DiscbotConfiguration>{
            ...baseConfig
        })
        const config: IConfig = Config.makeProperty(_config);
        container.constant('config', config);
        return config;
    }

    protected initDatabase() {
        if ( ! existsSync(paths.databaseFile) ) {
            writeFileSync(paths.databaseFile, '');
        }
        const database = Database.sqlite(paths.databaseFile, paths.migrationsDir);
        database.migrateLatest();
        container.constant('database', database);
        return database;
    }

    protected initCommandoClient() {
        const client = new CommandoClient({
            owner                 : this.config('ownerId'),
            unknownCommandResponse: false,
            commandPrefix         : '--'
        });
        client
            .on('ready', () => this.log.verbose('client: ready'))
            .on('disconnect', (event: CloseEvent) => {
                let log = `client: disconnected (${event.code}) ${event.reason}`
                if ( event.wasClean ) {
                    this.log.notice(log)
                } else {
                    this.log.crit(log)
                }
            })
            .on('debug', (info: string) => this.log.debug(info))
            .on('warn', (info: string) => this.log.warn(info))
            .on('error', (err: Error) => {
                this.log.error(`${err.name}: ${err.message}`, err.stack)
                process.exit(1)
            })
            .on('message', msg => {
                let author = msg.author;
                this.log.data(`:message: <author:${author.username}> ${msg.content}`);
                msg.channel.send('I have seen your message')
            })
        container.constant('client', client);
        return client
    }

    public initCli(config: CliConfig = {}): Cli {
        cli.config(<CliConfig>_.merge({
            commands: {
                onMissingArgument: 'help'
            }
        }, config));
        cli
            .helper('input')
            .helper('output')
            .helper('help', { app: { title: 'Discbot' }, addShowHelpFunction: true, showOnError: true, option: { enabled: true } })
            .helper('verbose', { option: { key: 'v', name: 'verbose' } })

        cli.use(DatabasePlugin, {})

        return cli;
    }

    public async initCommando(groups: CommandGroup[] | Function[] | string[][]): Promise<CommandoClient> {
        this.client = this.initCommandoClient();
        await this.client.setProvider(new Sqlite3SettingProvider(this.database));
        this.client.registry
            .registerGroups(groups)
            .registerDefaults()
            .registerTypesIn(paths.typesDir)
            .registerCommandsIn(paths.commandsDir)
        return Promise.resolve(this.client)
    }

    public async login(): Promise<CommandoClient> {
        await this.client.login(this.config('token'));
        this.log.debug(`authenticated :: token(${this.config('token')}) user(${this.client.user.id}) username(${this.client.user.username})`)
        this.log.silly(`authenticated`, this.client.user)
        return Promise.resolve(this.client);
    }
}

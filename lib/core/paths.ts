import { resolve } from "path";
import { homedir } from "os";

const configDir    = resolve(homedir(), '.discbot')
export const paths = {
    homeDir     : configDir,
    databaseFile: resolve(configDir, 'database.sqlite'),
    rcFiles     : [ resolve(__dirname, '..', '.discbotrc'), resolve(homedir(), '.discbotrc'), resolve(configDir, '.discbotrc') ],

    migrationsDir: resolve(__dirname, '../database/migrations'),
    commandsDir  : resolve(__dirname, '../commando/commands'),
    typesDir     : resolve(__dirname, '../commando/types'),
}

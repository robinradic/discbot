

import { IConfigProperty } from "@radic/util";

export interface DiscbotConfiguration {
    token?: string
    clientId?:string
    clientSecret?:string
    ownerId?:string
}

export interface IConfig extends IConfigProperty {

}

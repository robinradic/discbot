
import { CommandoClient, SettingProvider } from 'discord.js-commando'
import { Guild } from "discord.js";
import { Database } from "radical-console-database";
import { Settings } from "../models/settings";

export class Sqlite3SettingProvider extends SettingProvider {
    client:CommandoClient

    constructor(protected db:Database){
        super();
        Settings.query().select('*').from('settings')
    }

    public init(client: CommandoClient): Promise<void> {
        this.client = client;

        return super.init(client);
    }

    public clear(guild: Guild | string): Promise<void> {
        return super.clear(guild);
    }

    public destroy(): Promise<void> {
        return super.destroy();
    }

    public get(guild: Guild | string, key: string, defVal?: any): any {
        return super.get(guild, key, defVal);
    }

    public getGuildID(guild: Guild | string): string {
        return super.getGuildID(guild);
    }

    public remove(guild: Guild | string, key: string): Promise<any> {
        return super.remove(guild, key);
    }

    public set(guild: Guild | string, key: string, val: any): Promise<any> {
        return super.set(guild, key, val);
    }
}

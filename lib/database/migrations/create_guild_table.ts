"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *
 * @param {Knex} knex
 * @returns {*}
 */
exports.up = function (knex) {
    return knex.schema
        .createTable('settings', function (table) {
            table.increments('guild').primary();
            table.text('settings');
        });
};
/**
 *
 * @param {Knex} knex
 * @returns {Knex.SchemaBuilder}
 */
exports.down = function (knex) {
    return knex.schema
        .dropTableIfExists('settings')
};

import { AbstractModel } from "radical-console-database";
import { JsonSchema } from 'objection'

export class Settings extends AbstractModel {
    readonly guild: number
             settings: any

    public static all(){
        Settings.query().select('*').from('settings').execute()
    }

    static tableName = 'settings';

    static get jsonSchema(): JsonSchema {
        return {
            type      : 'object',
            required  : [ 'name', 'service', 'method', 'secret' ],
            properties: {
                guild   : { type: 'integer' },
                settings: { type: 'object', default: {} }
            }
        }
    }
}

#!/usr/bin/env node
require('module-alias/register')
import { CliConfig, command, CommandArguments, inject, Log, OutputHelper } from 'radical-console'
import { Bootstrapper,IConfig } from "../lib";
import * as ora from 'ora'

@command('discbot')
export default class DiscbotCmd {

    @inject('cli.helpers.output')
    out: OutputHelper;

    @inject('cli.log')
    log: Log

    @inject('config')
    config:IConfig

    async handle(args: CommandArguments, argv: string[]) {
        const spinner:ora.Ora = this.out.spinner('Connecting...').start()
        const client = await bs.initCommando([
            ['info', 'Informational Commands']
        ])
        await bs.login()
        let msg = `Connected as ${client.user.username} (${client.user.id})`;
        spinner.succeed(msg);
        this.log.data(msg);

        return new Promise((res,rej) => client.on('disconnect', (event:CloseEvent) => event.wasClean ? res() : rej(`${event.code}: ${event.reason}`)))
    }
}


const bs = Bootstrapper.boot();
const cli = bs.initCli();
cli.start(__filename)
